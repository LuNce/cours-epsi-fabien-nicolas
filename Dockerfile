FROM ubuntu

RUN apt-get update && apt-get install -y python3 python3-pip
RUN pip3 install flask
COPY server.py server.py
EXPOSE 8000
ENTRYPOINT ["python3", "server.py"]